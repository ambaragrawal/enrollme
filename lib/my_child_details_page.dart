import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'fire.dart';

class MyChildDetailsPage extends StatefulWidget {
  @override
  _MyChildDetailsPageState createState() => _MyChildDetailsPageState();
}

class _MyChildDetailsPageState extends State<MyChildDetailsPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String name;

  QuerySnapshot childs;

  Fire fire = new Fire();

  _addDialog() {
    addDialog(context);
  }

  _add() {
    Navigator.of(context).pop();
    Map<String, String> childData = <String, String>{
      'name': this.name
    };
    fire.addChild(childData).then((result) {
      _snackBar();
    }).catchError((e) {
      print(e);
    });
  }

  _snackBar() {
    final snackBar = new SnackBar(
      content: new Text('Yay! child is Added!')
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  Future<bool> addDialog(BuildContext context) async {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(
            'Add Child',
            style: new TextStyle(
              fontSize: 15.0
            )
          ),
          content: new Column(
            children: <Widget>[
              new TextField(
                decoration: new InputDecoration(
                  hintText: 'Child Name'
                ),
                onChanged: (value) {
                  this.name = value;
                }
              )
            ]
          ),
          actions: <Widget>[
            new FlatButton(
              child: new Text('Add'),
              textColor: Colors.blue,
              onPressed: _add
            )
          ],
        );
      }
    );
  }

  _getChilds() {
    fire.getChilds().then((QuerySnapshot results) {
      setState(() {
        childs = results;
      });
    });
  }

  @override
  void initState() {
    _getChilds();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        title: new Text('My Child Details'),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.refresh),
            onPressed: _getChilds,
          )
        ],
      ),
      body: _childList(),
      floatingActionButton: new FloatingActionButton(
        child: new Icon(Icons.add),
        onPressed: _addDialog
      ),
    );
  }

  Widget _childList() {
    if(childs != null) {
      return ListView.builder(
        itemCount: childs.documents.length,
        padding: EdgeInsets.all(5.0),
        itemBuilder: (context, i) {
          return new ListTile(
            title: new Text(
              'Child Name'
            ),
            subtitle: new Text(
              childs.documents[i].data['name']
            ),
          );
        }
      );
    } else {
      return new Center(
        child: new Text(
          'Loading, Please wait...',
          style: new TextStyle(
            fontSize: 32.0
          )
        )
      );
    }
  }
}