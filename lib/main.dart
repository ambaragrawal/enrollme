import 'package:flutter/material.dart';
import 'auth.dart';
import 'root_page.dart';
import 'my_details_page.dart';
import 'my_child_details_page.dart';
import 'calendar_page.dart';
import 'policies_page.dart';
import 'contact_us_page.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Enrollme',
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: 'Nunito',
      ),
      home: new RootPage(auth: new Auth()),
      routes: <String, WidgetBuilder> {
        '/myDetails': (BuildContext context) => new MyDetailsPage(),
        '/myChildDetails': (BuildContext context) => new MyChildDetailsPage(),
        '/calendar': (BuildContext context) => new CalendarPage(),
        '/policies': (BuildContext context) => new PoliciesPage(),
        '/contactUs': (BuildContext context) => new ContactUsPage(),
      },
    );
  }
}