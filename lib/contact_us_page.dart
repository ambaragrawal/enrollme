import 'package:flutter/material.dart';
import 'dart:async';
import 'package:url_launcher/url_launcher.dart';

class ContactUsPage extends StatefulWidget {
  @override
  _ContactUsPageState createState() => _ContactUsPageState();
}

class _ContactUsPageState extends State<ContactUsPage> {
  _email() async {
    const url = 'mailto:contact@enrollme.com';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _call() async {
    const url = 'tel:+1 555 010 999';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text('Contact Us')
      ),
      body: new Container(
        child: new Center(
          child: new Column(
            children: <Widget>[
              new SizedBox(height: 48.0),
              buildPage().first,
              new SizedBox(height: 24.0),
              buildContactButtons().first,
              new SizedBox(height: 48.0),
              buildPage().last,
              new SizedBox(height: 24.0),
              buildContactButtons().last
            ]
          )
        )
      )
    );
  }

  List<Widget> buildPage() {
    final email = new Text(
      'Email Us',
      style: new TextStyle(
        fontSize: 26.0
      ),
    );

    final call = new Text(
      'Call Us',
      style: new TextStyle(
        fontSize: 26.0
      ),
    );

    return [
      email,
      call,
    ];
  }

  List<Widget> buildContactButtons() {
    final emailButton = new Padding(
      padding: new EdgeInsets.symmetric(
        vertical: 16.0
      ),
      child: new Material(
        borderRadius: new BorderRadius.circular(5.0),
        shadowColor: Colors.blueAccent.shade100,
        elevation: 5.0,
        child: new MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: _email,
          color: Colors.blueAccent,
          child: new Text(
            'contact@enrollme.com',
            style: new TextStyle(
              color: Colors.white
            )
          )
        )
      ),
    );

    final callButton = new Padding(
      padding: new EdgeInsets.symmetric(
        vertical: 16.0
      ),
      child: new Material(
        borderRadius: new BorderRadius.circular(5.0),
        shadowColor: Colors.blueAccent.shade100,
        elevation: 5.0,
        child: new MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: _call,
          color: Colors.blueAccent,
          child: new Text(
            '+1 555 010 999',
            style: new TextStyle(
              color: Colors.white
            )
          )
        )
      ),
    );

    return [
      emailButton,
      callButton
    ];
  }
}