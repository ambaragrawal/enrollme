import 'package:flutter/material.dart';
import 'auth.dart';
import 'user.dart';

class HomePage extends StatefulWidget {
  HomePage({this.auth, this.onSignedOut});
  final BaseAuth auth;
  final VoidCallback onSignedOut;

  @override
  State<StatefulWidget> createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {

  User _user = new User();

  @override
  void initState() {
    super.initState();
    widget.auth.currentUserProfile().then((user) {
      setState(() {
        _user = user;
      });
    });
  }

  String getFirstCharOfEmail() {
    if (_user.email != null) {
      return _user.email[0].toUpperCase();
    }
  }

  void _signOut() async {
    try {
      await widget.auth.signOut();
      widget.onSignedOut();
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Enrollme'),
        actions: <Widget>[
          new FlatButton(
            child: new Text(
              'Logout',
              style: new TextStyle(
                fontSize: 17.0,
                color: Colors.white
              )
            ),
            onPressed: _signOut,
          )
        ],
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: new Text(
                'User Name',
                style: new TextStyle(
                  fontSize: 18.0
                )
              ),
              accountEmail: new Text(
                '${_user.email}',
                style: new TextStyle(
                  fontSize: 18.0
                )
              ),
              currentAccountPicture: new CircleAvatar(
                backgroundColor: Colors.white,
                child: new Text(
                  '${getFirstCharOfEmail()}',
                  style: new TextStyle(
                    fontSize: 24.0
                  )
                )
              ),
            ),
            new ListTile(
              title: new Text(
                'My Details',
                style: new TextStyle(
                  fontSize: 18.0
                )
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).pushNamed('/myDetails');
              },
            ),
            new Divider(),
            new ListTile(
              title: new Text(
                'My Child Details',
                style: new TextStyle(
                  fontSize: 18.0
                )
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).pushNamed('/myChildDetails');
              },
            ),
            new Divider(),
            new ListTile(
              title: new Text(
                'Calendar',
                style: new TextStyle(
                  fontSize: 18.0
                )
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).pushNamed('/calendar');
              },
            ),
            new Divider(),
            new ListTile(
              title: new Text(
                'Policies',
                style: new TextStyle(
                  fontSize: 18.0
                )
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).pushNamed('/policies');
              },
            ),
            new Divider(),
            new ListTile(
              title: new Text(
                'Contact Us',
                style: new TextStyle(
                  fontSize: 18.0
                )
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).pushNamed('/contactUs');
              },
            ),
          ],
        )
      ),
      body: new Container(
        child: new Center(
          child: new Column(
            children: <Widget>[
              new SizedBox(height: 48.0),
              buildPage().first,
              new SizedBox(height: 48.0),
              buildPage().last,
              new SizedBox(height: 56.0),
              buildLogoutButton().first
            ]
          )
        )
      )
    );
  }

  List<Widget> buildPage() {
    final welcome = new Text(
      'Welcome',
      style: new TextStyle(
        fontSize: 32.0
      ),
    );

    final user = new Text(
      '${_user.email}',
      style: new TextStyle(
        fontSize: 26.0
      ),
    );

    return [
      welcome,
      user,
    ];
  }

  List<Widget> buildLogoutButton() {
    final logoutButton = new Padding(
      padding: new EdgeInsets.symmetric(
        vertical: 16.0
      ),
      child: new Material(
        borderRadius: new BorderRadius.circular(5.0),
        shadowColor: Colors.blueAccent.shade100,
        elevation: 5.0,
        child: new MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: _signOut,
          color: Colors.blueAccent,
          child: new Text(
            'Log Out',
            style: new TextStyle(
              color: Colors.white
            )
          )
        )
      ),
    );

    return [
      logoutButton
    ];
  }
}