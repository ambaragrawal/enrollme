import 'package:flutter/material.dart';

class PoliciesPage extends StatelessWidget {
  PoliciesPage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text('Policies')
      ),
      body: new Column(
        children: <Widget>[
          new Padding(
            padding: new EdgeInsets.all(8.0),
            child: new Text(
              'Any of the information we collect from you may be used in one of the following ways:',
              style: new TextStyle(fontSize: 18.0)
            )
          ),
          new ListTile(
            leading: new Text('1'),
            title: new Text(
              'To personalize your experience (your information helps us to better responsd to your individula needs'
            ),
          ),
          new ListTile(
            leading: new Text('2'),
            title: new Text(
              'To improve our website (we continually stive to improve our website offering based on the information and feedback we receive from you)'
            ),
          ),
          new ListTile(
            leading: new Text('3'),
            title: new Text(
              'Your information, whether public or private, will not be sold, exchanged, transferred, or given to any other company for any reason whatsover, without your consent, other than for the express purpose of delivering the purchased product or service requested by the customer'
            ),
          ),
          new ListTile(
            leading: new Text('4'),
            title: new Text(
              'The email address your provide for order processing, may be used to send you information and updates pertaining to your order, in addtion to receving occasional company news, updates, related product or service information, etc'
            )
          ),
          new ListTile(
            leading: new Text('5'),
            title: new Text(
              'To administer a contest, promotion, survey or other site feature'
            )
          )
        ],
      )
    );
  }
}