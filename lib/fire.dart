import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'dart:async';

class Fire {
  bool isSignedIn() {
    if (FirebaseAuth.instance.currentUser() != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<void> addChild(childData) async {
    if (isSignedIn()) {
      Firestore.instance.collection('child')
        .add(childData)
        .catchError((e) {
          print('Error: $e');
        });
    } else {
      print('You need to be signed in!');
    }
  }

  Future<QuerySnapshot> getChilds() async {
    return await Firestore.instance.collection('child')
      .getDocuments();
  }
}