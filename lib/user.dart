class User {
  String email;

  String get userEmail { 
    return email; 
  } 
    
  set userEmail(String _email) { 
    this.email = _email; 
  } 
}