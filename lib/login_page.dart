import 'package:flutter/material.dart';
import 'auth.dart';

class LoginPage extends StatefulWidget {
  LoginPage({this.auth, this.onSignedIn});
  final BaseAuth auth;
  final VoidCallback onSignedIn;

  @override
  State<StatefulWidget> createState() => new _LoginPageState();
}

enum FormType {
  login,
  register
}

class _LoginPageState extends State<LoginPage> {
  
  final formKey = new GlobalKey<FormState>();

  String _email;
  String _password;
  FormType _formType = FormType.login;

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit() async {
    if (validateAndSave()) {
      try {
        if(_formType == FormType.login) {
          String userId = await widget.auth.signInWithEmailAndPassword(
            _email,
            _password
          );
          print('Signed in: $userId');
        } else {
          String userId = await widget.auth.createUserWithEmailAndPassword(
            _email,
            _password
          );
          print('Registered user: $userId');
        }
        widget.onSignedIn();
      }
      catch (e) {
        print('Error: $e');
      }
    }
  }

  void moveToRegister() {
    formKey.currentState.reset();
    setState(() {
      _formType = FormType.register;
    });
  }

  void moveToLogin() {
    formKey.currentState.reset();
    setState(() {
      _formType = FormType.login;
    });
  }

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('assets/logo.png'),
      )
    );

    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Enrollme')
      ),
      body: new SafeArea(
        top: false,
        bottom: false,
        child: new Form(
          key: formKey,
          child: new ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              new SizedBox(height: 24.0),
              logo,
              new SizedBox(height: 48.0),
              buildInputs().first,
              new SizedBox(height: 8.0),
              buildInputs().last,
              new SizedBox(height: 24.0),
              buildSubmitButtons().first,
              buildSubmitButtons().last,
            ]
          )
        )
      )
    );
  }
  
  List<Widget> buildInputs() {
    final email = new TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: new InputDecoration(
        hintText: 'Email',
        contentPadding: new EdgeInsets.fromLTRB(
          20.0,
          10.0,
          20.0,
          10.0
        ),
        border: new OutlineInputBorder(
          borderRadius: new BorderRadius.circular(5.0) 
        )
      ),
      validator: (value) => value.isEmpty ? 'Email can\'t be empty' : null,
      onSaved: (value) => _email = value,
    );

    final password = new TextFormField(
      autofocus: false,
      obscureText: true,
      decoration: new InputDecoration(
        hintText: 'Password',
        contentPadding: new EdgeInsets.fromLTRB(
          20.0,
          10.0,
          20.0,
          10.0
        ),
        border: new OutlineInputBorder(
          borderRadius: new BorderRadius.circular(5.0) 
        )
      ),
      validator: (value) => value.isEmpty ? 'Password can\'t be empty' : null,
      onSaved: (value) => _password = value,
    );

    return [
      email,
      password,
    ];
  }

  List<Widget> buildSubmitButtons() {
    final loginButton = new Padding(
      padding: new EdgeInsets.symmetric(
        vertical: 16.0
      ),
      child: new Material(
        borderRadius: new BorderRadius.circular(5.0),
        shadowColor: Colors.blueAccent.shade100,
        elevation: 5.0,
        child: new MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: validateAndSubmit,
          color: Colors.blueAccent,
          child: new Text(
            'Log In',
            style: new TextStyle(
              color: Colors.white
            )
          )
        )
      ),
    );

    final createAccountButton = new Padding(
      padding: new EdgeInsets.symmetric(
        vertical: 16.0
      ),
      child: new Material(
        borderRadius: new BorderRadius.circular(5.0),
        shadowColor: Colors.blueAccent.shade100,
        elevation: 5.0,
        child: new MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: validateAndSubmit,
          color: Colors.blueAccent,
          child: new Text(
            'Create Account',
            style: new TextStyle(
              color: Colors.white
            )
          )
        )
      ),
    );

    final toggleToCreateAccount = new FlatButton(
      child: new Text(
        'Create an account',
        style: TextStyle(
          color: Colors.black54
        )
      ),
      onPressed: moveToRegister,
    );

    final toggleToLogin = new FlatButton(
      child: new Text(
        'Have an account? Login',
        style: TextStyle(
          color: Colors.black54
        )
      ),
      onPressed: moveToLogin,
    );

    if (_formType == FormType.login) {
      return [
        loginButton,
        toggleToCreateAccount
      ];
    } else {
      return [
        createAccountButton,
        toggleToLogin
      ];
    }
  }
}