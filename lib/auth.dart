import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'user.dart';

abstract class BaseAuth {
  Future<String> signInWithEmailAndPassword(String email, String password);
  Future<String> createUserWithEmailAndPassword(String email, String password);
  Future<String> currentUser();
  Future<void> signOut();
  Future<User> currentUserProfile();
  Future<bool> isSignedIn();
}

class Auth implements BaseAuth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  Future<String> signInWithEmailAndPassword(String email, String password) async {
    FirebaseUser user = await _firebaseAuth.signInWithEmailAndPassword(
      email: email,
      password: password
    );
    return user.uid;
  }

  Future<String> createUserWithEmailAndPassword(String email, String password) async {
    FirebaseUser user = await _firebaseAuth.createUserWithEmailAndPassword(
      email: email,
      password: password
    );

    return user.uid;
  }

  Future<String> currentUser() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    if (user != null) {
      return user.uid;
    }
  }

  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }

  Future<User> currentUserProfile() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    if (user != null) {
      User _user = new User();
      _user.email = user.email;
      return _user;
    }
  }

  Future<bool> isSignedIn() async {
    if (_firebaseAuth.currentUser() != null) {
      return true;
    } else {
      return false;
    }
  }
}