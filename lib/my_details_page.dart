import 'package:flutter/material.dart';

class MyDetailsPage extends StatelessWidget {
  MyDetailsPage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text('My Details')
      ),
      body: new Center(
        child: new Text(
          'My Details',
          style: new TextStyle(
            fontSize: 32.0
          )
        )
      )
    );
  }
}